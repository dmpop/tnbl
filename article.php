<?php include 'inc/header.php';

//Read the requested file if it exists. Otherwise, exit with an error message.
if (file_exists($ARTICLES_DIR . DIRECTORY_SEPARATOR . $id . '.md')) {
    $content = file_get_contents($ARTICLES_DIR . DIRECTORY_SEPARATOR . $id . '.md');
} else {
    exit("<h1 style='text-align: center;'>404 ¯\_(⊙_ʖ⊙)_/¯</h1>");
}

$link_id = basename($ARTICLES_DIR . DIRECTORY_SEPARATOR . $id . '.md');
$Parsedown = new ParsedownExtra();

$reads_file = 'reads' . DIRECTORY_SEPARATOR . $id . '.reads';
// If $reads_file exists, increment views count by 1
if (file_exists($reads_file)) {
    $reads_count = fgets(fopen($reads_file, 'r'));
    // Increment count only if $_COOKIE['noreads'] is not set
    if (!isset($_COOKIE['noreads'])) {
        $reads_count++;
        @file_put_contents($reads_file, $reads_count);
    }
} else {
    // otherwise, create $reads_file and set $views_count to 0
    @file_put_contents($reads_file, '1');
    $reads_count = 1;
}
?>

<div class="h-entry"><?php echo $Parsedown->text($content); ?></div>
<hr>
<div style="text-align:center; font-style: italic; color: gray; margin-top: 2em;"><span style="margin-right: 1em;">Published: <?php echo substr($link_id, 0, strpos($link_id, "_")); ?></span><?php echo L::reads . ": " . $reads_count; ?></div>
<div style="text-align:center; margin-bottom: 2.5em; margin-top: 2em;"><a href="mailto:<?php echo $AUTHOR_EMAIL; ?>" class="btn" style="margin-right: 0.5em; color: #3399ff;"><?php echo L::comment_btn; ?></a><a href="<?php echo $BLOG_URL; ?>" class="btn back"><?php echo L::back_btn; ?></a></div>

<?php
if (!empty($GOAT_COUNTER)) {
    echo "<script data-goatcounter='https://$GOAT_COUNTER.goatcounter.com/count' async src='//gc.zgo.at/count.js'></script>";
}

include 'inc/footer.php';
?>