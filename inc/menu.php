<?php
$articles = glob($ARTICLES_DIR . DIRECTORY_SEPARATOR . '*.md');
rsort($articles);

$years = array();
foreach ($articles as $article) {
	$year = substr(basename($article), 0, 4);
	array_push($years, $year);
}
$years = array_unique($years);
?>
<form style="vertical-align: middle; display: inline-block;" method='POST' action='index.php'>
	<select class="btn" style="width: 5em;" name="year" onchange="form.submit()">
		<option value="Default"><?php echo L::year_lst; ?></option>
		<?php
		foreach ($years as $year) {
			echo "<option value='$year'>$year</option>";
		}
		?>
	</select>
</form>
<form style="vertical-align: middle; margin-left: 0.3em; display: inline-block;" method="POST" action="index.php">
	<select class="btn" name="page" style="min-width: 7em;" onchange="javascript:location.href = this.value;">
		<option value="Default"><?php echo L::page_lst; ?></option>
		<?php
		$pages = glob($PAGES_DIR . DIRECTORY_SEPARATOR . '*.md');
		foreach ($pages as $page) {
			$path_parts = pathinfo($page);
			$filename = $path_parts['filename'];
			$content = file_get_contents($page);
			$lines = explode("\n", $content);
			$title = substr($lines[0], 2); // Extract the title by removing preceding "#"
			echo "<option value='./page.php?id=$filename'>$title</option>";
		}
		?>
	</select>
</form>

<?php

// Find all files or a specific file if $_GET['q'] is set
if (isset($_GET['q'])) {
	$articles = array();
	$all_articles = glob($ARTICLES_DIR . DIRECTORY_SEPARATOR . "*.md");
	foreach ($all_articles as $article) {
		$text = file_get_contents($article);
		if (stripos($text, htmlentities($_GET['q'])) !== FALSE) {
			array_push($articles, $article);
		}
	}
} elseif (isset($_POST['year'])) {
	$articles = glob($ARTICLES_DIR . DIRECTORY_SEPARATOR . $_POST['year'] . "*.md");
	$y = $_POST['year'];
} else {
	$articles = glob($ARTICLES_DIR . DIRECTORY_SEPARATOR . date('Y') . "*.md");
	$y = date('Y');
}
?>

<a class="btn" style="margin-left: 0.3em;" href="<?php echo $BLOG_URL; ?>guestbook.php"><?php echo L::gb_guestbook; ?>
</a>
<a class="btn" style="margin-left: 0.3em; <?php if (isset($_COOKIE['noreads'])) {
												echo 'color: #ff3333;';
											} ?>" href="<?php echo $BLOG_URL; ?>reads.php"><?php echo L::reads; ?>
</a>