<?php
include('config.php');
?>

<!-- AUTHOR -->
<div class="card" style="margin-top: 3em;">
  <div class="about-me h-card">
    <img class="author-image u-photo" src="./img/author.png" alt="<?php echo $BLOG_AUTHOR; ?>" height="75" width="75" />
    <div class="author-info">
      <h3 class="author-name p-author"><a class="u-url u-uid" href="<?php echo $BLOG_URL; ?>"> <?php echo $BLOG_AUTHOR; ?></a></h3>
      <p class="p-note"><?php echo $AUTHOR_ABOUT; ?></p>
    </div>
  </div>
</div>

<footer class="footer">
    <?php
    echo $BLOG_FOOTER;
    ?>
    <a style="vertical-align: middle; margin-left: 0.5em" href="<?php echo $BLOG_URL; ?>rss.php">RSS</a>
</footer>
</div>

</body>

</html>
