<?php
include 'inc/header.php';

if (!isset($_GET['chrono'])) {
  rsort($articles);
}


// Find articles with the "pinned" string in their file names
// and move them to the top of the $articles array
foreach ($articles as $index => $item) {
  if (strpos($item, "pinned") !== false) {
    unset($articles[$index]);
    array_unshift($articles, $item);
  }
}

?>
<h3 style="font-family: Lora; text-align: center; margin-bottom: 2em;">&mdash;
  <?php if (isset($y)) {
    echo $y . " # ";
  }
  echo count($articles) . " " . L::articles; ?>
  &mdash;</h2>
  <?php
  foreach ($articles as $article) {
    $link_id = basename($article, '.md');
    $article = file_get_contents($article);
    $title = implode("\n", array_slice(explode("\n", $article), 0, 1));
    $summary = implode("\n", array_slice(explode("\n", $article), 1, 3));
    $Parsedown = new ParsedownExtra();

    if ($SHOW_SUMMARY) {
      echo '<a href="article.php?id='. $link_id . '">' . $Parsedown->text($title) . '</a>';
      echo '<div class="card">' . $Parsedown->text($summary) . '<div style="text-align: right; margin-bottom: 0em;"><a href="article.php?id=' . $link_id . '" class="btn">' . L::read_more . '</a></div></div>';
      echo '<div style="text-align: right;"><span style="font-size: 90%; color: gray;">Published: ' . substr($link_id, 0, strpos($link_id, "_")) . '</span></div>';
    } else {
      echo '<div>' . $Parsedown->text($article) . '<span style="font-size: 90%; color: gray;">Published: ' . substr($link_id, 0, strpos($link_id, "_")) . '</span></div>';
    };
  };
  ?>

  <?php include 'inc/footer.php'; ?>