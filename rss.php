<?php
require './lib/FeedWriter/Item.php';
require './lib/FeedWriter/Feed.php';
require './lib/FeedWriter/RSS2.php';
require './lib/parsedown.php';
require './config.php';

date_default_timezone_set('UTC');

use \FeedWriter\RSS2;

$RSS2Feed = new RSS2;

$RSS2Feed->setTitle($BLOG_TITLE)
    ->setLink($BLOG_URL)
    ->setDescription($BLOG_DESCRIPTION)
    ->setChannelElement('language', $LANG)
    ->setSelfLink($BLOG_URL . 'rss.php')
    ->setAtomLink($BLOG_URL . 'rss.php', 'hub')
    ->addGenerator()
    ->setImage($BLOG_TITLE, $BLOG_URL, $BLOG_URL . 'img/favicon.png');

$files = glob($ARTICLES_DIR . DIRECTORY_SEPARATOR . "*.md");
rsort($files);

foreach ($files as $article) {
    $time= date('H:i:s', filemtime($article)); // File modification time is used as publication time
    $link_id = pathinfo($article, PATHINFO_FILENAME);
    $content = file_get_contents($article);
    $lines = explode("\n", $content);
    $title = substr($lines[0], 2); // Extract the title by removing the preceding "#"
    $summary_raw = implode("\n", array_slice($lines, 1, 2)); // Extract the first two lines as a summary
    $Parsedown = new Parsedown();
    $summary = $Parsedown->line($summary_raw);
    $newItem = $RSS2Feed->createNewItem();

    $item_date_time = substr($link_id, 0, strpos($link_id, "_")) . ' ' . $time;
    $pub_date_time = strtotime($item_date_time);

    $newItem->setTitle($title)
        ->setLink($BLOG_URL . 'article.php?id=' . $link_id)
        ->setID($BLOG_URL . 'article.php?id=' . $link_id)
        ->setDate($pub_date_time)
        ->setDescription($summary)
        ->setAuthor($BLOG_AUTHOR, $AUTHOR_EMAIL);

    $RSS2Feed->addItem($newItem);
}

$RSS2Feed->printFeed();
